"use strict";
/**
 * @author wangchenzhao
 * @date 2018/11/26
 *
 * 参数检验规则
 * 参考地址 https://github.com/node-modules/parameter#rule
 */
module.exports = {
    templateReq: {
        user_name: {type: 'string', required: true},
        pwd: {type: 'string', required: true},
    },
    templateReq1: {
        user_name: {type: 'string', required: true},
        token: {type: 'string', required: true},
    }
}
