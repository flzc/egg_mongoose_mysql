"use strict";
/**
 * @author wangchenzhao
 * @date 2018/11/21
 */
const enumResType = {
    //成功
    ok_code: 200,

    // 提示msg
    prompt_err_code: 300,

    // 参数失败
    param_err_code: 400,

    // Token失败
    token_err_code: 500,

    // 系统错误
    system_err_code: 600
};
module.exports = {
    enumResType: enumResType,
    /**
     * 统一响应
     */
    resMsg: {
        newOk: function (data = [], msg = '成功') {
            return {res_code: enumResType.ok, msg, data};
        },
        newError: function (msg = '失败', code, data = []) {
            if (!code)
                code = enumResType.prompt_err_code;
            return {res_code: code, msg, data}
        }
    },
    /**
     * 自定义异常处理
     */
    customError: {
        promptError: function (message) {
            let e = new Error(message);
            e.name = 'promptError';
            throw e;
        }
    }
}