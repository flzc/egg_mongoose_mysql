"use strict";
/**
 * 公共服务
 * @author wangchenzhao
 * @date 2018/11/20
 */
const Service = require('egg').Service;

class template extends Service {

    constructor(param) {
        super(param);
        this.mysql = this.app.database.exam_mysql;
    }
}

module.exports = template;