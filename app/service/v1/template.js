"use strict";
/**
 *
 * @author wangchenzhao
 * @date 2018/11/20
 */
const Service = require('egg').Service;

class template extends Service {

    constructor(param) {
        super(param);
        this.mysql = this.app.database.exam_mysql;
    }

    async getList(token_info,user_name) {
        const {app, mysql} = this;
        //let data = await  mysql.query(sql,[]);
        return {token_info,user_name};
    }

    async postList(user_name, pwd) {
        const {app, ctx, mysql} = this;
        //let data = await  mysql.query(sql,[]);
        let user_info = ctx.helper.createToken({user_name, pwd});
        return user_info;
    }
}

module.exports = template;