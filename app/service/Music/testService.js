"use strict";
/**
 *
 * @author wangchenzhao
 * @date 2018/11/20
 */
const Service = require('egg').Service;

class testService extends Service {

    constructor(param) {
        super(param);
        this.mysql = this.app.database.exam_mysql;
    }

    async getList() {
        const {ctx} = this;
        console.log(ctx.model.test)
        const result = ctx.model.Music.Test.create({"userName":"Emma"});
        console.log(result)
        return ctx.model.Music.Test.find({"userName":"Emma"})
    }
}

module.exports = testService;