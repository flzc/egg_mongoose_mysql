"use strict";
/**
 * @author wangchenzhao
 * @date 2018/11/21
 */
module.exports = (version) => {
    return function (rule) {
        return async function checkParams(ctx, next) {
            const {enumResType} = ctx;
            try {
                if (!rule) {
                    let rule_key = ctx.path.substr(ctx.path.lastIndexOf('/') + 1) + 'Req';
                    rule = ctx.app.validate[version][rule_key];
                }
                let data = ctx.method == 'POST' ? ctx.request.body : ctx.query;
                ctx.validate(rule, data);
            } catch (e) {
                ctx.body = ctx.resMsg.newError("参数校验失败", enumResType.param_err_code, e.errors);
                return;
            }
            await next();
        };
    }
};