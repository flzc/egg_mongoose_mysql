"use strict";
/**
 * token 检验
 * @author wangchenzhao
 * @date 2018/11/21
 */
module.exports = (token) => {

    return async function checkToken(ctx, next) {
        const {enumResType} = ctx;
        try {
            let token = ctx.method == 'POST' ? ctx.request.body.token : ctx.query.token;
            let result = ctx.helper.checkToken(token);
            if (result.res_code != 200) {
                ctx.body = result;
                return;
            }
            ctx.__token_info = result || {};
        } catch (e) {
            ctx.body = ctx.resMsg.newError( 'token验证失败',enumResType.token_err_code);
            return;
        }
        await next();
    };
};