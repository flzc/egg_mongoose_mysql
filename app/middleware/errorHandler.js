"use strict";
/**
 * 统一异常处理
 * @author wangchenzhao
 * @date 2018/11/20
 */

module.exports = options => {
    return async function errorHandler(ctx, next) {
        const {enumResType} = ctx;
        try {
            await next();
        } catch (e) {
            if (e.name == 'promptError') {
                ctx.body = ctx.resMsg.newError(e.message, enumResType.prompt_err_code);
            } else {
                console.error(e)
                ctx.body = ctx.resMsg.newError('系统发生错误，请联系管理人员', enumResType.system_err_code);
            }
        }
    };
};