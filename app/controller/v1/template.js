"use strict";
/**
 * @author wangchenzhao
 * @date 2018/11/21
 */
const Controller = require('egg').Controller;

class template extends Controller {

    /**
     * this赋值 便捷处理
     * @param param
     */
    constructor(param) {
        super(param);
        this.mysql = this.app.database.exam_mysql;
        this.resMsg = this.ctx.resMsg;
    }



    async postList() {
        const {app, resMsg, mysql, ctx, service} = this;
        let token_info = ctx.__token_info;

        let user_name = ctx.request.body.user_name;
        let pwd = ctx.request.body.pwd;

        let results = await service.v1.template.postList(user_name,pwd);

        ctx.body = resMsg.newOk(results,'获取列表成功');
    }

    async getList() {
        const {app, resMsg, mysql, ctx, service} = this;
        let token_info = ctx.__token_info;

        let user_name = ctx.query.user_name;


        let results = await service.v1.template.getList(token_info,user_name);
        ctx.body = resMsg.newOk(results,'获取列表成功');
    }

}

module.exports = template;