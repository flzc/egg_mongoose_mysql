"use strict";
/**
 * @author wangchenzhao
 * @date 2018/11/21
 */
const Controller = require('egg').Controller;

class testController extends Controller {

    /**
     * this赋值 便捷处理
     * @param param
     */
    constructor(param) {
        super(param);
        this.mysql = this.app.database.exam_mysql;
        this.resMsg = this.ctx.resMsg;
    }


    async getList() {
        const {app, resMsg, mysql, ctx, service} = this;
        const result = await service.music.testService.getList();
        ctx.body = resMsg.newOk(result,'获取列表成功');
    }

}

module.exports = testController;