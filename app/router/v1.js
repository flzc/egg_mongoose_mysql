"use strict";
/**
 * @author wangchenzhao
 * @date 2018/11/20
 */
module.exports = app => {
    const {router, validate, controller} = app;

    // v1校验规则
    const rules = validate.v1;

    // 路由统一前缀
    const newRouter = router.namespace('/v1/api');
    // 路由中间价
    const checkParams = app.middleware.checkParams('v1');
    const checkToken = app.middleware.checkToken;


    // 请求
    newRouter.get('/music/template',  controller.music.testController.getList);
    newRouter.post('/template', checkParams(), controller.v1.template.postList);
    newRouter.get('/template', checkParams(rules.templateReq1), checkToken(), controller.v1.template.getList);

}