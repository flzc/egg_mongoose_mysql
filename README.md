

#### 安装教程
1. npm -i -d        安装
2. npm run dev      启动开发环境
3. npm run start    启动生成环境


#### app目录说明
*  app/controller                       控制器
*  app/controller/v1                    v1版本api  (顺延，v2,v3等版本)
*  app/extend/application.js            app扩展 rules参数检验规则
*  app/extend/context.js                ctx扩展 resMsg统一响应处理
*  app/extend/helper.js                 helper扩展 日期，token处理等常用函数
*  app/middleware                       中间件
*  app/middleware/checkParams.js        中间件--统一参数检验
*  app/middleware/checkToken.js         中间件--统一token检验
*  app/middleware/errorHandler.js       中间件--全局异常处理
*  app/public                           静态目录
*  app/router                           路由
*  app/router/v1.js                     v1版本路由  (顺延，v2,v3等版本)
*  app/schedule                         定时任务
*  app/service                          主目录
*  app/service/v1                       v1版本服务  (顺延，v2,v3等版本)
*  app/service/common                   公共服务(第三方服务器调用，微信支付，各版本通用服务等)

#### config目录说明
*  config/config.default.js             默认配置 （公共配置）
*  config/config.local.js               开发配置
*  config/config.prod.js                生成配置
*  config/plugin.js                     插件配置

#### test目录说明
*  test                                  单元测试


#### app.js说明
*  app.js                                启动前配置

