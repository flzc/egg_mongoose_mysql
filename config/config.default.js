"use strict";
/**
 * @author wangchenzhao
 * @date 2018/11/20
 */
// 引入egg-mongoose
const mongoose = require('mongoose');
module.exports = appInfo=> {
    return {
        keys:'my-cookie-secret-key',
        security:{
            csrf:false,
            ignoreJSON: true,
        },
         // 添加 view 配置，nunjucks是一个用来在服务器端渲染HTML的插件，用npm 安装即可
        view:{
            defaultViewEngine: 'nunjucks',
            mapping: {
              '.tpl': 'nunjucks',
            },
          },
          mongoose:{
            url: 'mongodb://127.0.0.1:27017/test',
            //链接到本地的MongoDB，mongoTest是我本地数据库的名字，根据自己数据库名字进行填写即可
            options: {},
            // loadModel: true,
            // app: true,
            // agent: false
          },
        domainWhiteList: [
            'http://localhost:9000',
            'http://localhost:7000',
            'http://localhost:3000',
            'http://m.tl100.com',
            'https://m.tl100.com'
        ],
        cors:{
            origin:'*',
            allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH'
        },
        public_dir : appInfo.baseDir + '/app/public',
        middleware: [ 'errorHandler' ],
    }
}

